import 'package:flutter/material.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'Audit.dart';
import 'ahwal.dart';
//import 'package:intl/intl.dart';

class OAudit extends StatefulWidget{
  
  const OAudit({super.key});
  

  @override
  State<OAudit> createState() => _OAuditState();
}

class _OAuditState extends State<OAudit>{
  
  DateTime? _startDate;
  DateTime? _endDate;

  Future<void> _selectStartDate(BuildContext context) async {
    final DateTime? picked = await showDatePicker(
      context: context,
      initialDate: _startDate ?? DateTime.now(),
      firstDate: DateTime(2010),
      lastDate: DateTime(2101),
    );
    if (picked != null && picked != _startDate) {
      setState(() {
        _startDate = picked;
        startdate.text=_startDate.toString();
      });
    }
  }

  Future<void> _selectEndDate(BuildContext context) async {
    final DateTime? picked = await showDatePicker(
      context: context,
      initialDate: _endDate ?? DateTime.now(),
      firstDate: DateTime(2010),
      lastDate: DateTime(2101),
    );
    if (picked != null && picked != _endDate) {
      setState(() {
        _endDate = picked;
        enddate.text=_endDate.toString();
      });
    }
  }

  

  Widget fetchMilk(String code,String date,String session,String day ) {

     DateTime startDate=DateTime.parse(startdate.text);
    DateTime endDate=DateTime.parse(enddate.text);

    if (code.isEmpty) {
    return const Text(
      "0",
      style: TextStyle(color: Colors.black, fontWeight: FontWeight.bold, fontSize: 20),
    );
  }

  return FutureBuilder<QuerySnapshot<Map<String, dynamic>>>(
    future: FirebaseFirestore.instance
        .collection('Collection')
        .doc(code)
        .collection("milkCollection")
        .where("date", isGreaterThanOrEqualTo: startDate)
        .where("date", isLessThanOrEqualTo: endDate)
        .get(),
    builder: (BuildContext context, AsyncSnapshot<QuerySnapshot<Map<String, dynamic>>> snapshot) {
      if (snapshot.connectionState == ConnectionState.waiting) {
        return const CircularProgressIndicator();
      }
      if (snapshot.hasError) {
        return Text(
          '${snapshot.error}',
          style: const TextStyle(color: Colors.red, fontWeight: FontWeight.w600, fontSize: 5),
        );
      }
      if (!snapshot.hasData || snapshot.data!.docs.isEmpty) {
        return const Text(
          '0',
          style: TextStyle(color: Colors.indigo, fontWeight: FontWeight.w600, fontSize: 20),
        );
      }

      var totalMilk = 0.0; // Initialize total milk
      var milkDocs = snapshot.data!.docs;

      // Iterate through all documents and sum up milk values for the correct session
      for (var doc in milkDocs) {
        var sess = doc.get('session');
        var fieldValue = doc.get('milk');
        
        // Check if the session matches the desired session
        if (sess == session) {
          totalMilk += fieldValue;
        }
      }

      // Display the total milk for the session
      return Text(
        '$totalMilk',
        style:const TextStyle(color: Colors.indigo, fontWeight: FontWeight.w600, fontSize: 20),
      );
    },
  );
}


  

  //var dateFormat=DateFormat('dd-MM-yyyy').format(DateTime.now());

 var startdate=TextEditingController(text:"${DateTime(DateTime.now().year,DateTime.now().month,DateTime.now().day,)}"); //DateFormat('dd/MM/yyyy').format(DateTime.now()));
  var enddate=TextEditingController(text: "${DateTime(DateTime.now().year,DateTime.now().month,DateTime.now().day,)}");//DateFormat('dd/MM/yyyy').format(DateTime.now()));


fetchTotalMilk(String code) async {
  DateTime startDate=DateTime.parse(startdate.text);
  DateTime endDate=DateTime.parse(enddate.text);
  
  var collectionRef = FirebaseFirestore.instance.collection("Collection").doc(code).collection("milkCollection");

  
  var querySnapshot = await collectionRef.where("date", isGreaterThanOrEqualTo: startDate)
                                          .where("date", isLessThanOrEqualTo: endDate)
                                          .get();

  
  if (querySnapshot.docs.isNotEmpty) {
    
    double totalMilk = querySnapshot.docs.fold(0, (previousValue, doc) => previousValue + doc["milk"]);

    return totalMilk;
  } else {
    return 0;
  }
}


  

  @override
  Widget build(BuildContext context){

    var screenSize=MediaQuery.of(context).size;
   // var time=DateTime(DateTime.now().year,DateTime.now().month,DateTime.now().day,);
   // int temp=time.hour;

   //String date="${time.day}" + " - " + "${time.month}" + " - " + "${time.year}";
   String code;
    
    return Scaffold(
      appBar: AppBar(
        title: const Text("कोरेश्वर डेअरी",style: TextStyle(color: Colors.white,fontSize: 25),),
        leading:  IconButton(
            onPressed: () {
              setState(() {
                Navigator.pop(context);
              });
            },
            icon: const Icon(Icons.arrow_back,color: Colors.white,size: 30,),
            ),
            actions: [
             
          IconButton(
            onPressed: () {
              Navigator.of(context).push(MaterialPageRoute(builder: (context) {
            return PdfPreviewPage(startdate.text,enddate.text);
          }));
            },
            icon: const Icon(Icons.picture_as_pdf,color: Colors.white,size: 30,),
          ),
          const SizedBox(width: 10,),
          
        ],
        backgroundColor: Colors.green


      ),

      body:
        SingleChildScrollView(
          child: Padding(
            padding: const EdgeInsets.all(20),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                const Text(
                  "  तारीख",
                  style: TextStyle(color: Colors.black, fontWeight: FontWeight.bold, fontSize: 20),
                ),
                 Row(
                    children: [
          
                         
                  SizedBox(
                    height: screenSize.height / 12,
                  width: screenSize.width / 2.66,
                  child: TextField(
                  controller: startdate,
                  decoration: InputDecoration(
                    border: const OutlineInputBorder(),
                     suffixIcon: IconButton(
                      onPressed: ()=> _selectStartDate(context),
                      icon: const Icon(Icons.calendar_month_outlined,color: Colors.indigo,size: 28,),
                    )
                    
                  ),
                  keyboardType: TextInputType.number,
                ),
                  ),
                  SizedBox(width: screenSize.width/20),
          
                  const Text("ते",style: TextStyle(color: Colors.black,fontSize: 22),),
                  SizedBox(width: screenSize.width/20),
          
                 SizedBox(
                    height: screenSize.height / 12,
                  width: screenSize.width / 2.66,
                  child: TextField(
                  controller: enddate,
                  decoration: InputDecoration(
                    border: const OutlineInputBorder(),
                    suffixIcon: IconButton(
                      onPressed: ()=> _selectEndDate(context),
                      icon: const Icon(Icons.calendar_month_outlined,color: Colors.indigo,size: 28,),
                    )
                    
                  ),
                  keyboardType: TextInputType.number,
                ),
                  ),
                
                  ]
                 
                ),
          
              
                SizedBox(height: screenSize.height / 55),
                SingleChildScrollView(child: 
                Container(
                  child:
          
                 StreamBuilder(
                stream: FirebaseFirestore.instance.collection('Members').snapshots(),
                builder: (BuildContext context, AsyncSnapshot<QuerySnapshot<Map<String, dynamic>>> snapshot) {
          if (snapshot.connectionState == ConnectionState.waiting) {
            return const Center(child: CircularProgressIndicator());
          }
          if (snapshot.hasError) {
            return Center(child: Text('Error: ${snapshot.error}'));
          }
          if (!snapshot.hasData || snapshot.data!.docs.isEmpty) {
            return const Center(child: Text('No data available', style: TextStyle(fontSize: 20, fontStyle: FontStyle.normal, color: Colors.indigo)));
          }
          var screenSize=MediaQuery.of(context).size;
          return SizedBox(
            height:screenSize.height/1.4, 
            
            child:
          ListView.builder(
                              //shrinkWrap: true,
                              itemCount: snapshot.data!.docs.length,
                              itemBuilder: (context,index){
                                  code=snapshot.data?.docs[index]["code"];
                                  return GestureDetector(
                                    onTap:() {
                                      String name="${snapshot.data?.docs[index]["name"]}";
                                                String code="${snapshot.data?.docs[index]["code"]}";
                                                //String animalType="${snapshot.data?.docs[index]["animal type"]}";
                                                Navigator.push(context,MaterialPageRoute(builder:(context)=>Audit(code.toString(),name.toString())));
                                    },
                                    child: Container(
                                      margin: const EdgeInsets.symmetric(vertical: 8,horizontal: 5),
                                      decoration: BoxDecoration(border: Border.all(color: Colors.black26,),borderRadius: BorderRadius.circular(20)),
                                      child:
                                              
                                      Padding(padding:const EdgeInsets.all(10),
                                      child:
                                              
                                      Row(
                                        
                                        children: [
                                          const CircleAvatar(
                                            child: //Icon(Icons.person,color: Colors.black38,)
                                            Icon(Icons.person,color: Colors.black38,)
                                          ,
                                              
                                          ),
                                         const SizedBox(width: 15,),
                                          Column(
                                            crossAxisAlignment: CrossAxisAlignment.start,
                                            children: [
                                              Text("${snapshot.data?.docs[index]["name"]}",style: const TextStyle(color: Colors.black,fontSize: 19,fontWeight: FontWeight.w500),),
                                              Text("कोड : ${snapshot.data?.docs[index]["code"]}",style: const TextStyle(color: Colors.black,fontWeight: FontWeight.w500,),),
                                              Text("गाय-म्हैस : ${snapshot.data?.docs[index]["animal type"]}",style: const TextStyle(color: Colors.black,fontWeight: FontWeight.w500,),),
                                            ],
                                          ),
                                          
                                          const Spacer(),
                                          
                                          FutureBuilder(
                                              future: fetchTotalMilk(code),
                                              builder: (context, snapshot) {
                                                if (snapshot.connectionState == ConnectionState.waiting) {
                                                  return const CircularProgressIndicator();
                                                } else if (snapshot.hasError) {
                                                  return Text("Error: ${snapshot.error}");
                                                } else {
                                                  double totalMilk=double.parse(snapshot.data.toString());
                                              
                                                  return Text("${totalMilk.toStringAsFixed(1)} लिटर",style: const TextStyle(color: Colors.indigo,fontSize: 20,fontWeight: FontWeight.w600),); // Return the Text widget from the future
                                                }
                                              },
                                                    ),
                                          
                                        ],
                                      )
                                                                    
                                      )
                                    ),
                                  );
                },
          )
            
          );
                }
              ),
                )
            )
                
                
                
                
               
              ],
            ),
          ),
        ),
    

      

    );
  }
}


