
import 'package:flutter/material.dart';
import 'package:firebase_auth/firebase_auth.dart';
//import 'Homepage.dart';
import 'login.dart';

class Signup extends StatefulWidget{
  const Signup({super.key});

  @override
  State<Signup> createState()=>_SignupState(); 
}

class _SignupState extends State<Signup>{

  var _textcontroller1=TextEditingController();
  var _textcontroller2=TextEditingController();

  bool valid=false;

  @override
  void dispose(){
    _textcontroller1.dispose();
    _textcontroller2.dispose();
    super.dispose();
  }

  String username1="s";
  String password1="8828";

  Future<void> _signup()async{
        if(_textcontroller1.text=="" && _textcontroller2.text==""){
                  setState(() {
                    valid=true;
                  });
                      
         }
                  /*else if(password1==_textcontroller2.text && username1==_textcontroller1.text){
                    _textcontroller1.clear();
                    _textcontroller2.clear();
                    valid=false;
                    Navigator.push(context,MaterialPageRoute(builder:(context)=>HomePage()));

                  }*/
                  else{
                   // UserCredential? userCredential;
                   // var user=FirebaseAuth.instance.currentUser;
                    try{
                      await FirebaseAuth.instance.createUserWithEmailAndPassword(email:_textcontroller1.text.toString(), password: _textcontroller2.text.toString()).then((value) => Navigator.push(context,MaterialPageRoute(builder:(context)=>Login())) );
                      //await user!.sendEmailVerification();

                      
                    }
                    on FirebaseAuthException catch(ex){
                    //  var error=ex.code.toString();
                     var error=ex.code.toString();
                      if(error=="network-request-failed"){
                        error="इंटरनेट कनेक्शन तपासा !";
                      }
                      else{
                        error="योग्य इ-मेल,पासवर्ड प्रविष्ट करा !";
                      }
                       showDialog(context: context, 
                                builder: (ctx)=> AlertDialog(
                                  title: const Text("ERROR",style: TextStyle(color: Colors.red,fontWeight: FontWeight.bold),),
                                  content: Text("$error",style: TextStyle(fontSize: 18),),
                                  actions: [
                                    TextButton(onPressed: (){
                                      Navigator.of(ctx).pop();
                                    },
                                    child:const Text("OK",style: TextStyle(fontSize: 15,color: Colors.blue),)
                                    
                                    /*Container(
                                      color: Colors.green,
                                      padding: const EdgeInsets.all(14),
                                      child: const Text("OK",st),
                                    ),*/
                                    ),
                                  ],
                                )
                                
                                );
                    }
                  }
    }

  @override
  Widget build(BuildContext context){
    var screensize=MediaQuery.of(context).size;

    

    return Scaffold(
      appBar: AppBar(
         title: const Text("कोरेश्वर डेअरी",style: TextStyle(color: Colors.white,fontSize: 25),),
        automaticallyImplyLeading: false,
        backgroundColor: Colors.green

      ),
      body:
      Container(
        
        height:double.infinity,
        width:double.infinity,
        decoration:BoxDecoration(image:DecorationImage(image:const AssetImage("assets/abc.webp",),fit: BoxFit.fill)),
        child:
      Center(
        child: Container(
          height: screensize.height/2.2,
          width: screensize.width/1.5,
          decoration: BoxDecoration(color: Colors.white70,borderRadius: BorderRadius.all(Radius.circular(8)),border: Border.all(color: Colors.black38)),
          child: 
        Padding(padding:EdgeInsets.all(20),
        child: 
        Column(
          children: [
            TextField(
              controller: _textcontroller1,
              decoration: InputDecoration(
                border: OutlineInputBorder(),
                hintText: "इ-मेल प्रविष्ट करा",
                errorText: valid ? "योग्य इ-मेल प्रविष्ट करा":null,
                prefixIcon: Icon(Icons.mail)
              ),
            ),
            SizedBox(height: screensize.height/55,),
            TextField(
              controller: _textcontroller2,
              decoration: InputDecoration(
                border:const  OutlineInputBorder(),
                hintText: "सांकेतिक अंक प्रविष्ट करा",
                errorText: valid ? "वापरकर्ता /सांकेतिक अंक चुकीचा":null,
                prefixIcon: const Icon(Icons.password_outlined),
                //errorText: "Enter valid password"
              ),
              //keyboardType: TextInputType.number,
            ),
             SizedBox(height: screensize.height/40,),
             ElevatedButton(
              onPressed: ()=> _signup(),
                /*setState(() {
                  if(password1==_textcontroller2.text && username1==_textcontroller1.text){
                    _textcontroller1.clear();
                    _textcontroller2.clear();
                    valid=false;
                    Navigator.pop(context);

                  }
                  else{
                    valid=true;
                  }
                });
              },*/
             
              
              child: 
              const Text("खाते उघडा",style: TextStyle(color: Colors.white),),
              style: ElevatedButton.styleFrom(
                backgroundColor: Colors.green
              ),
              ),
               SizedBox(height: screensize.height/70,),

              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children:[
              const Text("जुने खाते आहे ?",style:TextStyle(fontSize: 15),),

               //SizedBox(height: screensize.height/130,),

              TextButton(
              onPressed: (){
                setState(() {
                  Navigator.pushReplacement(context, MaterialPageRoute(builder:(context)=>Login()));
                });
              }, 
              child: const Text("लॉगिन करा",style:TextStyle(color: Colors.blue,fontSize: 15),)
              )
                ],
              )  
             
          ],
        )
        )
        )
      )
      )
    );
}
}
