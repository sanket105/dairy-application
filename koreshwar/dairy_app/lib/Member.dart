//import 'package:firebase_core/firebase_core.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'AddMember.dart';
import 'Audit.dart';

class Member extends StatefulWidget{
  const Member({super.key});

  @override
  State<Member> createState()=>_MemeberState();
}

class _MemeberState extends State<Member>{


  String id="";

  //final TextEditingController _textcontroller=TextEditingController();
  deleteData(String index)async{
     FirebaseFirestore.instance.collection("Members").doc(index).delete();
  }
  
  
  

  @override
  Widget build(BuildContext context){
  
    
    var screensize=MediaQuery.of(context).size;
    return Scaffold(
         appBar: AppBar(
        title: const Text("कोरेश्वर डेअरी",style: TextStyle(color: Colors.white,fontSize: 25),),
        leading:  IconButton(
            onPressed: () {
              setState(() {
                Navigator.pop(context);
              });
            },
            icon:  Icon(Icons.arrow_back,color: Colors.white,size: 30,),
            ),
        backgroundColor: Colors.green


      ),

      body: 
      
      Padding(padding: const EdgeInsets.all(10),
      child:
      StreamBuilder(
                stream: FirebaseFirestore.instance.collection("Members").snapshots(),  
                builder:(context,snapshot){
                  if(snapshot.connectionState==ConnectionState.active){
                      if(snapshot.hasData && snapshot.data!.docs.isNotEmpty){
                          
                          return ListView.builder(
                            itemCount: snapshot.data!.docs.length,
                            itemBuilder: (context,index){
                                return 
                                GestureDetector(
                                  onTap:(){
                                    String name="${snapshot.data?.docs[index]["name"]}";
                                                String code="${snapshot.data?.docs[index]["code"]}";
                                                //String animalType="${snapshot.data?.docs[index]["animal type"]}";
                                                Navigator.push(context,MaterialPageRoute(builder:(context)=>Audit(code.toString(),name.toString())));
                                  },
                                  child: Container(
                                    margin: const EdgeInsets.symmetric(vertical: 8,horizontal: 5),
                                    decoration: BoxDecoration(border: Border.all(color: Colors.black26,),borderRadius: BorderRadius.circular(20)),
                                    child:
                                  
                                    Padding(padding:const EdgeInsets.all(10),
                                    child:
                                  
                                    Row(
                                      
                                      children: [
                                        const CircleAvatar(
                                          child: 
                                            Icon(
                                              Icons.person,color: Colors.black38,
                                            ),
                                        ),
                                        SizedBox(width: screensize.height/50,),
                                        Column(
                                          crossAxisAlignment: CrossAxisAlignment.start,
                                          children: [
                                            Text("${snapshot.data?.docs[index]["name"]}",style: const TextStyle(color: Colors.black,fontSize: 19,fontWeight: FontWeight.w500),),
                                            Text("कोड : ${snapshot.data?.docs[index]["code"]}",style: const TextStyle(color: Colors.black,fontWeight: FontWeight.w500,),),
                                            Text("गाय-म्हैस : ${snapshot.data?.docs[index]["animal type"]}",style: const TextStyle(color: Colors.black,fontWeight: FontWeight.w500,),),
                                          ],
                                        ),
                                        
                                        const Spacer(),
                                        
                                        
                                        IconButton(
                                          onPressed:(){
                                            
                                            showDialog(context: context, 
                                  builder: (ctx)=> AlertDialog(
                                    title: const Text("🗑 Delete",style: TextStyle(color: Colors.red,fontWeight: FontWeight.bold),),
                                    content:// Text("योग्य इ-मेल,पासवर्ड प्रविष्ट करा",style: TextStyle(fontSize: 18),),
                                    Text("${snapshot.data?.docs[index]["name"]} यांना काढून टाकायचे",style: const TextStyle(color: Colors.black,fontSize: 16,fontWeight: FontWeight.w500),),
                                    actions: [
                                      TextButton(onPressed: (){
                                        Navigator.of(ctx).pop();
                                        var id=snapshot.data?.docs[index]["code"];
                                        deleteData(id.toString());
                                      },
                                      child:const Text("काढून टाका",style: TextStyle(fontSize: 15,color: Colors.red),)
                                      
                                      /*Container(
                                        color: Colors.green,
                                        padding: const EdgeInsets.all(14),
                                        child: const Text("OK",st),
                                      ),*/
                                      ),
                                      TextButton(onPressed: (){
                                        Navigator.of(ctx).pop();
                                      },
                                      child:const Text("नको",style: TextStyle(fontSize: 15,color: Colors.blue),))
                                    ],
                                  )
                                  
                                  );
                                  
                                  
                                           /* ScaffoldMessenger.of(context).showSnackBar( SnackBar(
                                            content: TextButton(
                                              onPressed: () {
                                                var id=snapshot.data?.docs[index]["code"];
                                                deleteData(id.toString());
                                              },
                                              child: const Text("delete"),
                                            ),
                                            backgroundColor:Colors.white,
                                            elevation: 10,
                                            behavior: SnackBarBehavior.floating,
                                            margin:EdgeInsets.symmetric(vertical: 100,horizontal: 70),
                                              
                                              
                                            ));*/
                                            //var id=snapshot.data?.docs[index]["code"];
                                            //deleteData(id.toString());
                                          }, 
                                          icon: const Icon(Icons.delete)
                                        )
                                      ],
                                    )
                                                                 /* ListTile(
                                      leading: const CircleAvatar(
                                        child: Icon(Icons.person,color: Colors.black38,),
                                      ),
                                      title: Text("${snapshot.data?.docs[index]["name"]}",style: const TextStyle(color: Colors.black,fontSize: 19,fontWeight: FontWeight.w500),),
                                      subtitle: Text("कोड : ${snapshot.data?.docs[index]["code"]}",style: const TextStyle(color: Colors.black,fontWeight: FontWeight.w500,),),
                                  )*/
                                    )
                                  ),
                                );
                            }
                          
                          
                          );
                      }
                      else if(snapshot.hasError){
                        return Center(child: Text(snapshot.error.toString()),);
                      }
                      else{
                       
                        return const Center(child:Text("सभासद उपलब्ध नाही 😢",style:TextStyle(fontSize: 20,fontStyle: FontStyle.normal,color: Colors.indigo)));
                      }
                     
                  }
                  else{
                         
                        return const Center(child: CircularProgressIndicator(),);
                  }
                },
               
              )

    
      ) ,
      floatingActionButton: FloatingActionButton(
        onPressed: (){
          setState(() {
            Navigator.push(context,MaterialPageRoute(builder:(context)=>const AddMember()));
          });
        },
        tooltip: 'सभासद जोडा',
        
        hoverColor: Colors.indigo,
        backgroundColor: Colors.green,
        child: const Icon(Icons.add,color: Colors.white,),
        
      ),
      
    );
  }
}