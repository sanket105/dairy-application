import 'package:flutter/material.dart';
import 'package:cloud_firestore/cloud_firestore.dart';

class Collection extends StatefulWidget {
  const Collection({Key? key}) : super(key: key);

  @override
  State<Collection> createState() => __CollectionState();
}

class __CollectionState extends State<Collection> {
  var _textController1 = TextEditingController();
  var _textController2 = TextEditingController();


  Future<bool> checkDocumentExistence(String code,String date,String session) async {
    String day=date+"("+session+")";
  try {
    DocumentSnapshot documentSnapshot = await FirebaseFirestore.instance.collection("Collection").doc(code).collection("milkCollection").doc(day).get();
    return documentSnapshot.exists;
  } catch (e) {
   // print("Error checking document existence: $e");
    return false;
  }
}


  addData(String code, double milk, String date, String session) async {
    String day=date+"("+session+")";
  try {
    bool docPresence = await checkDocumentExistence(code,date,session);
    if (docPresence==true) {
      showDialog(
        context: context,
        builder: (ctx) => AlertDialog(
          title: const Text(
            "🚨 ALERT",
            style: TextStyle(color: Colors.red, fontWeight: FontWeight.bold),
          ),
          content: Text(
            "अगोदरच दूध घेतले आहे",
            style: const TextStyle(color: Colors.black, fontSize: 16, fontWeight: FontWeight.w500),
          ),
          actions: [
            TextButton(
              onPressed: () {
                Navigator.of(ctx).pop();
                FirebaseFirestore.instance.collection("Collection").doc(code).
    collection("milkCollection").doc(day).set({
      
      "date":DateTime(DateTime.now().year,DateTime.now().month,DateTime.now().day,),
      "session":session,
      "milk":milk,
    }).then((value) {
          ScaffoldMessenger.of(context).showSnackBar(const SnackBar(
            content: Text("यशस्वीरीत्या दूध घेतले !",textAlign: TextAlign.center,style: TextStyle(fontSize: 15,color: Colors.black,),) ,
            backgroundColor:Colors.white,
            elevation: 10,
            behavior: SnackBarBehavior.floating,
            margin:EdgeInsets.symmetric(vertical: 100,horizontal: 70),
            
            
          ));
      });
                
              },
              child: const Text(
                "बदल करायचा",
                style: TextStyle(fontSize: 15, color: Colors.red),
              ),
            ),
            TextButton(
              onPressed: () {
                Navigator.of(ctx).pop();
              },
              child: const Text(
                "नको",
                style: TextStyle(fontSize: 15, color: Colors.blue),
              ),
            )
          ],
        ),
      );
    } 
    else{
    FirebaseFirestore.instance.collection("Collection").doc(code).
    collection("milkCollection").doc(day).set({
      
      "date":DateTime(DateTime.now().year,DateTime.now().month,DateTime.now().day,),
      "session":session,
      "milk":milk,
    }).then((value) {
          ScaffoldMessenger.of(context).showSnackBar(const SnackBar(
            content: Text("यशस्वीरीत्या दूध घेतले !",textAlign: TextAlign.center,style: TextStyle(fontSize: 15,color: Colors.black,),) ,
            backgroundColor:Colors.white,
            elevation: 10,
            behavior: SnackBarBehavior.floating,
            margin:EdgeInsets.symmetric(vertical: 100,horizontal: 70),
            
            
          ));
      });}
  }catch(e){
      ScaffoldMessenger.of(context).showSnackBar(SnackBar(
            content: Text("$e !",textAlign: TextAlign.center,style: const TextStyle(fontSize: 15,color: Colors.black,),) ,
            backgroundColor:Colors.white,
            elevation: 10,
            behavior: SnackBarBehavior.floating,
            margin:const EdgeInsets.symmetric(vertical: 100,horizontal: 70),
            
            
      ));
      
  }
  }

  Widget fetchAnimal(String code) {
    if (code.isEmpty) {
      return const Text("गाय - म्हैस",style: TextStyle(color: Colors.indigo, fontSize: 22),);
    }

    return FutureBuilder<DocumentSnapshot>(
      future: FirebaseFirestore.instance.collection('Members').doc(code).get(),
      builder: (BuildContext context, AsyncSnapshot<DocumentSnapshot> snapshot) {
        if (snapshot.connectionState == ConnectionState.waiting) {
          return const CircularProgressIndicator();
        }
        if (snapshot.hasError) {
          return Text('Error: ${snapshot.error}');
        }
        if (!snapshot.hasData || !snapshot.data!.exists) {
          return const Text('सभासद नाही सापडला',style: TextStyle(color: Colors.indigo, fontSize: 22),);
        }
        var fieldValue = snapshot.data!.get('animal type');
        if(fieldValue=="गाय"){
          fieldValue="🐮 गाय ";
        }
        else{
          fieldValue="🐃 म्हैस ";
        }
        return Text('$fieldValue',style: const TextStyle(color: Colors.indigo, fontSize: 22),);
      },
    );
  }


  Widget fetchName(String code) {
    if (code.isEmpty) {
      return const Text("सभासदाचे नाव",style: TextStyle(color: Colors.black, fontWeight: FontWeight.bold, fontSize: 20),);
    }

    return FutureBuilder<DocumentSnapshot>(
      future: FirebaseFirestore.instance.collection('Members').doc(code).get(),
      builder: (BuildContext context, AsyncSnapshot<DocumentSnapshot> snapshot) {
        if (snapshot.connectionState == ConnectionState.waiting) {
          return const CircularProgressIndicator();
        }
        if (snapshot.hasError) {
          return Text('Error: ${snapshot.error}');
        }
        if (!snapshot.hasData || !snapshot.data!.exists) {
          return const Text('सभासद नाही सापडला',style: TextStyle(color: Colors.black, fontWeight: FontWeight.w600, fontSize: 20),);
        }
        var fieldValue = snapshot.data!.get('name');
        return Text('$fieldValue',style: const TextStyle(color: Colors.black, fontWeight: FontWeight.w600, fontSize: 20),);
      },
    );
  }


  bool valid1 = false;
  bool valid2 = false;

  @override
  Widget build(BuildContext context) {
    var time = DateTime.now();
    int temp = time.hour;
    String str = "";
    if (temp < 12) {
      str = "☀ सकाळ";
    } else {
      str = "🌙 संध्याकाळ";
    }
    var screenSize = MediaQuery.of(context).size;
    return Scaffold(
      appBar: AppBar(
        title: const Text(
          "कोरेश्वर डेअरी",
          style: TextStyle(color: Colors.white, fontSize: 25),
        ),
        leading: IconButton(
          onPressed: () {
            setState(() {
              Navigator.pop(context);
            });
          },
          icon: Icon(Icons.arrow_back, color: Colors.white, size: 30),
        ),
        backgroundColor: Colors.green,
      ),
      body: SingleChildScrollView(
        child: Padding(
          padding: const EdgeInsets.all(20),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              const Text(
                "  तारीख",
                style: TextStyle(color: Colors.black, fontWeight: FontWeight.bold, fontSize: 20),
              ),
              Container(
                height: screenSize.height / 20,
                width: screenSize.width / 1,
                alignment: Alignment.center,
                decoration: BoxDecoration(color: Colors.white, border: Border.all(color: Colors.black12)),
                child: Text(
                  "${time.day}/${time.month}/${time.year} ",
                  style: TextStyle(fontSize: 22, color: Colors.black),
                ),
              ),
              SizedBox(height: screenSize.height / 55),
              Container(
                height: screenSize.height / 20,
                width: screenSize.width / 1,
                alignment: Alignment.center,
                decoration: BoxDecoration(color: Colors.white, border: Border.all(color: Colors.black12)),
                child: Text("$str", style: TextStyle(color: Colors.indigo, fontSize: 22)),
              ),
              SizedBox(height: screenSize.height / 55),
              Container(
                height: screenSize.height / 20,
                width: screenSize.width / 1,
                alignment: Alignment.center,
                decoration: BoxDecoration(color: Colors.white, border: Border.all(color: Colors.black12)),
                child: fetchAnimal(_textController1.text.toString()),
              ),
              SizedBox(height: screenSize.height / 55),
              Container(
                height: screenSize.height / 20,
                width: screenSize.width / 1,
                alignment: Alignment.center,
                decoration: BoxDecoration(color: Colors.white, border: Border.all(color: Colors.black12)),
                child: fetchName(_textController1.text.toString()),
              ),
              SizedBox(height: screenSize.height / 50),
              const Text("  कोड", style: TextStyle(color: Colors.black, fontWeight: FontWeight.bold, fontSize: 20)),
              TextField(
                controller: _textController1,
                decoration: InputDecoration(
                  border: const OutlineInputBorder(),
                  errorText: valid1 ? "कोड प्रविष्ट करा" : null,
                ),
                keyboardType: TextInputType.number,
              ),
              SizedBox(height: screenSize.height / 50),
              const Text("  लिटर", style: TextStyle(color: Colors.black, fontWeight: FontWeight.bold, fontSize: 20)),
              TextField(
                controller: _textController2,
                decoration: InputDecoration(
                  border: const OutlineInputBorder(),
                  errorText: valid2 ? "दूध प्रविष्ट करा" : null,
                ),
                keyboardType: TextInputType.number,
              ),
              SizedBox(height: screenSize.height / 30),
              Row(
                children: [
                  ElevatedButton(
                    onPressed: () {
                      _textController1.clear();
                      _textController2.clear();
                    },
                    style: ElevatedButton.styleFrom(backgroundColor: Colors.orangeAccent.shade200),
                    child: const Text(
                      "काढून टाका",
                      style: TextStyle(fontSize: 22, color: Colors.white),
                    ),
                  ),
                  const Spacer(),
                  ElevatedButton(
                    onPressed: () {
                      if (_textController1.text.length <= 1 && _textController2.text.isEmpty) {
                        
                          setState(() {
                            valid1 = true;
                            valid2 = true;
                          });
                        
                      } else if (_textController1.text.length <= 1) {
                        
                          setState(() {
                            valid1 = true;
                          });
                        
                      } else if (_textController2.text.isEmpty) {
                        
                          setState(() {
                            valid2 = true;
                          });
                        
                      } else {
                        String date = "${time.day}" + " - " + "${time.month}" + " - " + "${time.year}";
                        String session = "eveningSession";
                        if (str == "☀ सकाळ") {
                          session = "morningSession";
                        }
                        double milk = double.parse(_textController2.text);
                        addData(_textController1.text, milk, date.toString(), session);
                        _textController1.clear();
                        _textController2.clear();
                       
                          setState(() {
                            valid1 = false;
                            valid2 = false;
                          });
                       
                      }
                    },
                    style: ElevatedButton.styleFrom(backgroundColor: Colors.green),
                    child: const Text(
                      "जतन  करा",
                      style: TextStyle(fontSize: 22, color: Colors.white),
                    ),
                  ),
                ],
              )
            ],
          ),
        ),
      ),
    );
  }
}

