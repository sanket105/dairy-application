import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'login.dart';
import 'Homepage.dart';

class User extends StatefulWidget{
  const User({super.key});

  @override
  State<User> createState()=>_UserState();

}

class _UserState extends State<User>{

  @ override
  Widget build(BuildContext context){
    return checkUser();
  }

  checkUser(){
    final user=FirebaseAuth.instance.currentUser;
    if(user!=null){
      return HomePage();
    }
    else{
      return Login();
    }
  
  }

}