
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
//import 'Member.dart';

class AddMember extends StatefulWidget{
  const AddMember({super.key});

  @override
  State<AddMember> createState() => _AddMemberState();

  
}

class _AddMemberState extends State<AddMember>{

    bool? checkedVal1=false;
    bool? checkedVal2=false;
    //var _textcontroller1=TextEditingController();
   // var temp=FirebaseFirestore.instance.collection("Members").
   // var temp;

    var name=TextEditingController();
    var code=TextEditingController();

    bool valid1=false;
    bool valid2=false;
    bool valid3=false;


    Future<int> getDocLength()async{
      var snapshot=await FirebaseFirestore.instance.collection("Members").get();

      return snapshot.docs.length;
    }

    var temp;

    Future<bool> checkDocumentExistence(String collectionName, String documentId) async {
  try {
    DocumentSnapshot documentSnapshot = await FirebaseFirestore.instance.collection(collectionName).doc(documentId).get();
    return documentSnapshot.exists;
  } catch (e) {
   // print("Error checking document existence: $e");
    return false;
  }
}
   



   addData(String name, String code, String animalType) async {
  try {
    bool docPresence = await checkDocumentExistence("Members", code.toString());
    if (docPresence) {
      showDialog(
        context: context,
        builder: (ctx) => AlertDialog(
          title: const Text(
            "🚨 ALERT",
            style: TextStyle(color: Colors.red, fontWeight: FontWeight.bold),
          ),
          content: Text(
            "कोड अगोदरच वापरला आहे",
            style: const TextStyle(color: Colors.black, fontSize: 16, fontWeight: FontWeight.w500),
          ),
          actions: [
            TextButton(
              onPressed: ()async {
                await FirebaseFirestore.instance.collection("Members").doc(code).set({
        "name": name,
        "code": code,
        "animal type": animalType,
      });
      ScaffoldMessenger.of(context).showSnackBar(
        const SnackBar(
          content: Text(
            "यशस्वीरीत्या सभासद जोडला !",
            textAlign: TextAlign.center,
            style: TextStyle(fontSize: 15, color: Colors.black),
          ),
          backgroundColor: Colors.white,
          elevation: 10,
          behavior: SnackBarBehavior.floating,
          margin: EdgeInsets.symmetric(vertical: 100, horizontal: 70),
        ),
      );
                Navigator.of(ctx).pop();
                
                
                
              },
              child: const Text(
                "बदल करायचा",
                style: TextStyle(fontSize: 15, color: Colors.red),
              ),
            ),
            TextButton(
              onPressed: () {
                Navigator.of(ctx).pop();
              },
              child: const Text(
                "नको",
                style: TextStyle(fontSize: 15, color: Colors.blue),
              ),
            )
          ],
        ),
      );
    } if(docPresence==false) {
      await FirebaseFirestore.instance.collection("Members").doc(code).set({
        "name": name,
        "code": code,
        "animal type": animalType,
      });
      ScaffoldMessenger.of(context).showSnackBar(
        const SnackBar(
          content: Text(
            "यशस्वीरीत्या सभासद जोडला !",
            textAlign: TextAlign.center,
            style: TextStyle(fontSize: 15, color: Colors.black),
          ),
          backgroundColor: Colors.white,
          elevation: 10,
          behavior: SnackBarBehavior.floating,
          margin: EdgeInsets.symmetric(vertical: 100, horizontal: 70),
        ),
      );
    }
  } catch (e) {
    ScaffoldMessenger.of(context).showSnackBar(SnackBar(
            content: Text("$e !",textAlign: TextAlign.center,style: const TextStyle(fontSize: 15,color: Colors.black,),) ,
            backgroundColor:Colors.white,
            elevation: 10,
            behavior: SnackBarBehavior.floating,
            margin:const EdgeInsets.symmetric(vertical: 100,horizontal: 70),
            
            
      ));
    
  }
}



    @override
    Widget build(BuildContext context){
      
      // temp=getDocLength();
     // _code.text=getDocLength().toString();
      var screensize=MediaQuery.of(context).size;
      return Scaffold(
           appBar: AppBar(
        title: const Text("कोरेश्वर डेअरी",style: TextStyle(color: Colors.white,fontSize: 25),),
        leading:  IconButton(
            onPressed: () {
              setState(() {
                Navigator.pop(context);
              });
            },
            icon:  Icon(Icons.arrow_back,color: Colors.white,size: 30,),
            ),
        backgroundColor: Colors.green


      ),

      body: 
      SingleChildScrollView(child: 
      Padding(
        padding:const EdgeInsets.all(20),
        child: 
  
        Column(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            SizedBox(height: screensize.height/50,),
          

            const Text("  सभासदाचे  नाव",style: TextStyle(color: Colors.black,fontWeight: FontWeight.bold,fontSize: 20),),
            
          
            TextField(
              controller: name,
              decoration: InputDecoration(
                border:const OutlineInputBorder(),
                hintText: "सभासदाचे नाव प्रविष्ट करा",
                errorText: valid2 ? "सभासदाचे नाव प्रविष्ट करा":null,
              ),
              keyboardType: TextInputType.name,
            ),
            

            SizedBox(height: screensize.height/25,),

            const Text("  कोड",style: TextStyle(color: Colors.black,fontWeight: FontWeight.bold,fontSize: 20),),
            
            TextField(
              controller: code,
              decoration: InputDecoration(
                border: const OutlineInputBorder(),
                hintText: "कोड प्रविष्ट करा",
                errorText: valid3 ? "कोड प्रविष्ट करा":null,
              ),
              keyboardType: TextInputType.number,
            ),
            
            SizedBox(height: screensize.height/25,),
            const Text("  गाय / म्हैस",style: TextStyle(color: Colors.black,fontWeight: FontWeight.bold,fontSize: 20),),
             SizedBox(height: screensize.height/55,),
            Row(children: [
            Checkbox(
              value: checkedVal1, 
              activeColor: Colors.green,
              onChanged:(newVal){
                setState(() {
                  checkedVal1=newVal;
                });
              }
              ),
              const Text("  गाय",style: TextStyle(color: Colors.black,fontSize: 20),),

              
            ],
            ),
             Row(children: [
            Checkbox(
              value: checkedVal2, 
              activeColor: Colors.green,
              onChanged:(newVal){
                setState(() {
                  checkedVal2=newVal;
                });
              }
              ),
              const Text("  म्हैस",style: TextStyle(color: Colors.black,fontSize: 20),),

              
            ],
            ),
            SizedBox(height: screensize.height/25,),
            Center(
              child: 
            SizedBox(
              width:screensize.width/2,
              height:screensize.height/20,
              
              child:
            ElevatedButton(
              onPressed: (){
                if ( name.text.isEmpty && code.text.length <=1) {
                        
                          setState(() {
                            valid3 = true;
                            valid2 = true;
                          });
                        
                      }
               
                 if(name.text.isEmpty ){
                    setState(() {
                      valid2=true;
                    });
                  }
                  if(code.text.length<=1){
                    setState(() {
                      valid3=true;
                    });
                  }
                  if( name.text.isNotEmpty && code.text.length>1){
                    String animalType="गाय";
                    if(checkedVal2==true){
                      animalType="म्हैस";
                    }
                    
                  addData(name.text.toString(), code.text.toString(), animalType,);
                    
                  
                 // _textcontroller1.clear();
                 
                name.clear();
                //code.clear();
                checkedVal1=false;
                checkedVal2=false;
                //valid1=false;
                setState(() {
                  valid2=false;
                valid3=false;
                });
                
                temp="${int.parse(code.text)+1}";
                code.text=temp;
                }
                //});
              },
              style: ElevatedButton.styleFrom(
                backgroundColor: Colors.green
              ),
              child: 
              const Text("जतन  करा",style: TextStyle(fontSize: 25,color: Colors.white),),
              
              
            ),
            ),
            )
            
          ]
        
      ),
             
      
      )
      )
      );
    }
}