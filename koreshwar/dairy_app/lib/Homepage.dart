
import 'package:dairy_app/forgotpass.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'Member.dart';
import 'Collection.dart';
import 'login.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'overallAudit.dart';

class HomePage extends StatefulWidget {
  const HomePage({super.key});

  @override
  State<HomePage> createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  void logOut() async {
    await FirebaseAuth.instance.signOut();
    Navigator.pushReplacement(context, MaterialPageRoute(builder: (context) => Login()));
  }

  Future<int> getNumberOfDocumentsInCollection(String collectionPath) async {
    QuerySnapshot snapshot = await FirebaseFirestore.instance.collection("Members").get();
    return snapshot.size;
  }


Future<Map<String, int>> getDocumentCounts(String session) async {
  Map<String, int> documentCounts = {};
 // DateTime date = DateTime(DateTime.now().year,DateTime.now().month,DateTime.now().day,);//"${DateTime(DateTime.now().year, DateTime.now().month, DateTime.now().day)}";
 String day="${DateTime.now().day} - ${DateTime.now().month} - ${DateTime.now().year}($session)";
  try {
    // Query Firestore for all documents matching the specified date and session
    QuerySnapshot<Map<String, dynamic>> querySnapshot = await FirebaseFirestore.instance
        .collectionGroup("milkCollection") // Query across all subcollections named "milkCollection"
        //.where("date", isEqualTo: date)
       //.where("session", isEqualTo: session)
        .get();

    // Iterate through the query snapshot to count documents for each code
    querySnapshot.docs.forEach((doc) {
      String code = doc.reference.parent.parent!.id; // Get the code from the document path
      if (!documentCounts.containsKey(code)) {
        documentCounts[code] = 0; // Initialize count to 0 if not already present
      }
      if(doc.id==day){
      documentCounts[code] = documentCounts[code]! + 1; // Increment count for the code
      }
    });

    return documentCounts;
  } catch (e) {
    // Handle any errors that occur during the operation
   // print("Error getting document counts: $e");
    return {}; // Return an empty map in case of error
  }
}


Future<double> getTotalMilk(String session) async {
  double totalMilk = 0;
  String day = "${DateTime.now().day} - ${DateTime.now().month} - ${DateTime.now().year}($session)";
  try {
    QuerySnapshot<Map<String, dynamic>> querySnapshot = await FirebaseFirestore.instance
        .collectionGroup("milkCollection")
        //.where("date", isEqualTo: day) // Filter by date
        //.where("session", isEqualTo: session) // Filter by session
        .get();

    querySnapshot.docs.forEach((doc) {
      if(doc.id==day){
      totalMilk += doc.data()["milk"] ?? 0; // Add milk value to totalMilk, defaulting to 0 if milk is null
      }
    });

    return totalMilk;
  } catch (e) {
    // Handle errors
   // print("Error calculating total milk: $e");
    return 0; // Return 0 if there's an error
  }
}

Future<double> fetchTotalMilk() async {
  double totalMilk = 0;
  String day1 = "${DateTime.now().day} - ${DateTime.now().month} - ${DateTime.now().year}(morningSession)";
   String day2 = "${DateTime.now().day} - ${DateTime.now().month} - ${DateTime.now().year}(eveningSession)";
  try {
    QuerySnapshot<Map<String, dynamic>> querySnapshot = await FirebaseFirestore.instance
        .collectionGroup("milkCollection")
        //.where("date", isEqualTo: day) // Filter by date
        //.where("session", isEqualTo: session) // Filter by session
        .get();

    querySnapshot.docs.forEach((doc) {
      if(doc.id==day1 || doc.id==day2){
      totalMilk += doc.data()["milk"] ?? 0; // Add milk value to totalMilk, defaulting to 0 if milk is null
      }
    });

    return totalMilk;
  } catch (e) {
    // Handle errors
   // print("Error calculating total milk: $e");
    return 0; // Return 0 if there's an error
  }
}














  

  @override
  Widget build(BuildContext context) {
    var time = DateTime.now();
    int temp = time.hour;
    String str = '';
    String session='';
    if (temp < 12) {
      str = "सकाळ";
      session='morningSession';
    } else {
      str = "संध्याकाळ";
      session="eveningSession";
    }
    var screensize = MediaQuery.of(context).size;
    // String date = "${time.day}" + " - " + "${time.month}" + " - " + "${time.year}";

    return FutureBuilder<int>(
      future: getNumberOfDocumentsInCollection("Members"),
      builder: (BuildContext context, AsyncSnapshot<int> snapshot) {
        if (snapshot.connectionState == ConnectionState.waiting) {
          return Scaffold(
            appBar: AppBar(
              title: const Text("कोरेश्वर डेअरी", style: TextStyle(color: Colors.white, fontSize: 25)),
              backgroundColor: Colors.green,
              iconTheme: const IconThemeData(color: Colors.white),
            ),
            body: const Center(
              child: CircularProgressIndicator(),
            ),
          );
        } else if (snapshot.hasError) {
          return Scaffold(
            appBar: AppBar(
              title: const Text("कोरेश्वर डेअरी", style: TextStyle(color: Colors.white, fontSize: 25)),
              backgroundColor: Colors.green,
              iconTheme: const IconThemeData(color: Colors.white),
            ),
            body: Center(
              child: Text('Error: ${snapshot.error}'),
            ),
          );
        } else {
          int size = snapshot.data!;
          return Scaffold(
            appBar: AppBar(
              title: const Text("कोरेश्वर डेअरी", style: TextStyle(color: Colors.white, fontSize: 25)),
              backgroundColor: Colors.green,
              iconTheme: const IconThemeData(color: Colors.white),
            ),
            drawer: Drawer(
              backgroundColor: const Color.fromARGB(255, 60, 148, 137),
              child: ListView(
                padding: EdgeInsets.zero,
                children: [
                  const DrawerHeader(
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.all(Radius.circular(20)),
                      color: Colors.green,
                      image: DecorationImage(
                        image: AssetImage("assets/abc.webp"),
                        fit: BoxFit.fill,
                      ),
                    ),
                    child: Stack(
                      children: [
                        Positioned(
                          bottom: 8.0,
                          left: 4.0,
                          child: Text(
                            "कोरेश्वर डेअरी",
                            style: TextStyle(fontSize: 20),
                          ),
                        )
                      ],
                    ),
                  ),
                  ListTile(
                    leading: Icon(Icons.home, size: 30, color: Colors.white),
                    title: const Text(
                      "पहिले पान",
                      style: TextStyle(fontSize: 18, color: Colors.white),
                    ),
                    onTap: () {
                      Navigator.pop(context);
                    },
                  ),
                  ListTile(
                    leading: Icon(Icons.password_outlined, size: 30, color: Colors.white),
                    title: const Text(
                      "सांकेतिक अंक बदला",
                      style: TextStyle(fontSize: 18, color: Colors.white),
                    ),
                    onTap: () {
                      Navigator.push(context, MaterialPageRoute(builder: (context) => ForgotPass()));
                    },
                  ),
                  ListTile(
                    leading: Icon(Icons.logout_outlined, size: 30, color: Colors.white),
                    title: const Text(
                      "बाहेर पडा",
                      style: TextStyle(fontSize: 18, color: Colors.white),
                    ),
                    onTap: () => logOut(),
                  ),
                ],
              ),
            ),
            bottomNavigationBar: BottomNavigationBar(
              unselectedItemColor: Colors.white,
              selectedItemColor: Colors.white,
              unselectedFontSize: 15,
              selectedFontSize: 15,
              items: [
                BottomNavigationBarItem(
                  icon: IconButton(
                    onPressed: () {
                      setState(() {
                        Navigator.push(context, MaterialPageRoute(builder: (context) => Member()));
                      });
                    },
                    icon:const Icon(Icons.people_alt_outlined, color: Colors.white, size: 30),
                  ),
                  label: "सभासद",
                ),
                BottomNavigationBarItem(
                  icon: IconButton(
                    onPressed: () {
                      setState(() {
                        Navigator.push(context, MaterialPageRoute(builder: (context) => Collection()));
                      });
                    },
                    icon:const Icon(Icons.takeout_dining_outlined, color: Colors.white, size: 30),
                  ),
                  label: "संकलन",
                ),
                BottomNavigationBarItem(
                  icon: IconButton(
                    onPressed: () {
                      setState(() {
                        Navigator.push(context, MaterialPageRoute(builder: (context) => OAudit()));
                      });
                    },
                    icon: const Icon(Icons.edit_document, color: Colors.white, size: 30),
                  ),
                  label: 'अहवाल',
                ),
              ],
              backgroundColor: Colors.green,
            ),
            body: SingleChildScrollView(
              child: Column(
                children: [
                  SizedBox(height: screensize.height / 53),
                  Row(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      SizedBox(width: screensize.width / 55),
                      Container(
                        height: screensize.height / 17,
                        width: screensize.width / 1.04,
                        alignment: Alignment.center,
                        decoration: BoxDecoration(
                          color: Colors.white,
                          borderRadius: BorderRadius.circular(40),
                          border: Border.all(color: Colors.black),
                        ),
                        child: Text(
                          "${time.day}/${time.month}/${time.year}         $str",
                          style:const TextStyle(fontSize: 30, fontWeight: FontWeight.bold, color: Colors.deepOrange),
                        ),
                      )
                    ],
                  ),
                  SizedBox(height: screensize.height / 53),
                  Row(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      SizedBox(width: screensize.width / 55),
                      Container(
                        height: screensize.height / 9.5,
                        width: screensize.width / 1.04,
                        alignment: Alignment.center,
                        decoration: BoxDecoration(
                          color: Colors.white,
                          borderRadius: BorderRadius.circular(40),
                          border: Border.all(color: Colors.black),
                        ),
                        child: Row(
                          children: [
                            SizedBox(width: screensize.width / 20),
                            Column(
                              children: [
                                Text(
                                  "$size",
                                  style: TextStyle(fontSize: 28, fontWeight: FontWeight.bold),
                                ),
                                SizedBox(height: screensize.height / 180),
                               const Text(
                                  "सक्रिय सभासद",
                                  style: TextStyle(fontSize: 22, fontWeight: FontWeight.normal),
                                )
                              ],
                            ),
                            Spacer(),
                            Column(
                              children: [
                               FutureBuilder<Map<String, int>>(
      future: getDocumentCounts(session),
      builder: (BuildContext context, AsyncSnapshot<Map<String, int>> snapshot) {
        if (snapshot.connectionState == ConnectionState.waiting) {
          return const CircularProgressIndicator(); // Or any loading indicator
        } else if (snapshot.hasError) {
          return Text('Error: ${snapshot.error}');
        } else {
          Map<String, int> documentCounts = snapshot.data!;
          int remainingMembers = size - (documentCounts.isNotEmpty ? documentCounts.values.reduce((a, b) => a + b) : 0);
          return Column(
            children: [
              Text(
                "$remainingMembers",
                style: const TextStyle(fontSize: 28, fontWeight: FontWeight.bold),
              ),
              SizedBox(height: screensize.height / 180),
              const Text(
                "बाकी सभासद",
                style: TextStyle(fontSize: 22, fontWeight: FontWeight.normal),
              )
            ],
          );
        }
      }
                               )
                               /* Text(
                                  "4",
                                  style: TextStyle(fontSize: 28, fontWeight: FontWeight.bold),
                                ),*/
                               /* SizedBox(height: screensize.height / 180),
                                Text(
                                  "बाकी सभासद",
                                  style: TextStyle(fontSize: 22, fontWeight: FontWeight.normal),
                                )*/
                              ],
                            ),
                            SizedBox(width: screensize.width / 20),
                          ],
                        ),
                      )
                    ],
                  ),
                  SizedBox(height: screensize.height / 53),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      SizedBox(width: screensize.width / 27),
                      Container(
                        height: screensize.height / 5.7,
                        width: screensize.width / 1.09,
                        alignment: Alignment.topCenter,
                        decoration: BoxDecoration(
                          color: Colors.white,
                          borderRadius: BorderRadius.circular(40),
                          border: Border.all(color: Colors.black),
                        ),
                        child: Column(
                          children: [
                           const Text(
                              "☀  सकाळ दूध  ☀",
                              style: TextStyle(fontSize: 30, color: Colors.indigo, fontWeight: FontWeight.bold),
                            ),
                            SizedBox(height: screensize.height / 25),
                            FutureBuilder<double>(
            future: getTotalMilk("morningSession"),
            builder: (BuildContext context, AsyncSnapshot<double> snapshot) {
              if (snapshot.connectionState == ConnectionState.waiting) {
                return const CircularProgressIndicator();
              } else if (snapshot.hasError) {
                return Text('Error: ${snapshot.error}');
              } else {
               //totMilk=0.0;
                double totalMilk = snapshot.data ?? 0;
                
                return Text(
                  "${totalMilk.toStringAsFixed(1)} लिटर", // Display total milk with 2 decimal places
                  style: const TextStyle(fontSize: 28, color: Colors.black, fontWeight: FontWeight.bold),
                );
              }
            },
          ),
                          ],
                        ),
                      ),
                    ],
                  ),
                  SizedBox(height: screensize.height / 53),
                  Row(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      SizedBox(width: screensize.width / 27),
                      Container(
                        height: screensize.height / 5.7,
                        width: screensize.width / 1.09,
                        alignment: Alignment.topCenter,
                        decoration: BoxDecoration(
                          color: Colors.white,
                          borderRadius: BorderRadius.circular(40),
                          border: Border.all(color: Colors.black),
                        ),
                        child: Column(
                          children: [
                            const Text(
                              "🌕 संध्याकाळ दूध 🌕",
                              style: TextStyle(fontSize: 30, color: Colors.indigo, fontWeight: FontWeight.bold),
                            ),
                            SizedBox(height: screensize.height / 25),
                            FutureBuilder<double>(
            future: getTotalMilk("eveningSession"),
            builder: (BuildContext context, AsyncSnapshot<double> snapshot) {
              if (snapshot.connectionState == ConnectionState.waiting) {
                return const CircularProgressIndicator();
              } else if (snapshot.hasError) {
                return Text('Error: ${snapshot.error}');
              } else {
                double totalMilk = snapshot.data ?? 0;
                
                return Text(
                  "${totalMilk.toStringAsFixed(1)} लिटर", // Display total milk with 2 decimal places
                  style: const TextStyle(fontSize: 28, color: Colors.black, fontWeight: FontWeight.bold),
                );
              }
            },
          ),
                          ],
                        ),
                      ),
                    ],
                  ),
                  SizedBox(height: screensize.height / 55),
                  Row(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      SizedBox(width: screensize.width / 27),
                      Container(
                        height: screensize.height / 5.7,
                        width: screensize.width / 1.09,
                        alignment: Alignment.topCenter,
                        decoration: BoxDecoration(
                          color: Colors.white,
                          borderRadius: BorderRadius.circular(40),
                          border: Border.all(color: Colors.black),
                        ),
                        child: Column(
                          children: [
                            const Text(
                              "🐄  एकूण दूध  🐄",
                              style: TextStyle(fontSize: 30, color: Colors.indigo, fontWeight: FontWeight.bold),
                            ),
                            SizedBox(height: screensize.height / 25),
                            FutureBuilder<double>(
            future: fetchTotalMilk(),
            builder: (BuildContext context, AsyncSnapshot<double> snapshot) {
              if (snapshot.connectionState == ConnectionState.waiting) {
                return const CircularProgressIndicator();
              } else if (snapshot.hasError) {
                return Text('Error: ${snapshot.error}');
              } else {
                double totalMilk = snapshot.data ?? 0;
                
                return Text(
                  "${totalMilk.toStringAsFixed(1)} लिटर", // Display total milk with 2 decimal places
                  style: const TextStyle(fontSize: 28, color: Colors.black, fontWeight: FontWeight.bold),
                );
              }
            },
          ),
                          ],
                        ),
                      ),
                    ],
                  )
                ],
              ),
            ),
          );
        }
      },
    );
  }
}
