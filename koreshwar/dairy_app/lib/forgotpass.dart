
//import 'package:dairy_app/ChangePass.dart';
import 'package:firebase_auth/firebase_auth.dart';
//import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/material.dart';

class ForgotPass extends StatefulWidget{
  const ForgotPass({super.key});

  @override
  State<ForgotPass> createState()=>_ForgotPassState(); 
}

class _ForgotPassState extends State<ForgotPass>{

  var _textcontroller1=TextEditingController();
//  var _textcontroller2=TextEditingController();

  bool valid=false;

 // var user=FirebaseAuth.instance.currentUser;


  //String? email=(FirebaseAuth.instance.currentUser.email)!.toString();

 


  @override
  void dispose(){
    _textcontroller1.dispose();
   // _textcontroller2.dispose();
    super.dispose();
  }

  String username1="s";
  String password1="8828";

  ChangePass(String email)async{
      if(email==""){
        setState(() {
          valid=true;
        });
      }
      else{

        
        if( FirebaseAuth.instance.currentUser==null ||  (FirebaseAuth.instance.currentUser?.email== email)  ){
          FirebaseAuth.instance.sendPasswordResetEmail(email: email);
          ScaffoldMessenger.of(context).showSnackBar(const SnackBar(
            content: Text("यशस्वीरीत्या इ-मेल पाठवला !",textAlign: TextAlign.center,style: TextStyle(fontSize: 15,color: Colors.black,),) ,
            backgroundColor:Colors.white,
            elevation: 10,
            behavior: SnackBarBehavior.floating,
            margin:EdgeInsets.symmetric(vertical: 100,horizontal: 70),
            
            
          ));
        }
        else{
          showDialog(context: context, 
                                builder: (ctx)=> AlertDialog(
                                  title: const Text("ERROR",style: TextStyle(color: Colors.red,fontWeight: FontWeight.bold),),
                                  content: Text("योग्य इ-मेल प्रविष्ट करा",style: TextStyle(fontSize: 18),),
                                  actions: [
                                    TextButton(onPressed: (){
                                      Navigator.of(ctx).pop();
                                    },
                                    child:const Text("ठीक आहे",style: TextStyle(fontSize: 15,color: Colors.blue),)
                                    
                                    /*Container(
                                      color: Colors.green,
                                      padding: const EdgeInsets.all(14),
                                      child: const Text("OK",st),
                                    ),*/
                                    ),
                                  ],
                                )
                                
                                );
                    
        }
      }
  }

  @override
  Widget build(BuildContext context){
    
    var screensize=MediaQuery.of(context).size;
    return Scaffold(
      appBar: AppBar(
         title: const Text("कोरेश्वर डेअरी",style: TextStyle(color: Colors.white,fontSize: 25),),
         leading: IconButton(
          onPressed: (){
            setState(() {
              Navigator.pop(context);
            });
          },
          icon: Icon(Icons.arrow_back,color: Colors.white,)
        ),
        backgroundColor: Colors.green

      ),
      body:
      Container(
        
        height:double.infinity,
        width:double.infinity,
        
        decoration:BoxDecoration(image:DecorationImage(image:const AssetImage("assets/abc.webp",),fit: BoxFit.fill)),
        child:
      Center(
        child: Container(
          height: screensize.height/4,
          width: screensize.width/1.5,
          decoration: BoxDecoration(color: Colors.white70,borderRadius: BorderRadius.all(Radius.circular(8)),border: Border.all(color: Colors.black38)),
          child: 
        Padding(padding:EdgeInsets.all(20),
        child: 
        Column(
          children: [
            TextField(
              controller: _textcontroller1,
              decoration: InputDecoration(
                border: OutlineInputBorder(),
                hintText: "इ-मेल प्रविष्ट करा",
                errorText: valid ? "योग्य इ-मेल प्रविष्ट करा":null,
                prefixIcon: Icon(Icons.mail)
              ),
            ),
         /*   Container(
              height:screensize.height/55 ,
              width: screensize.width/20,
              child: 
                Text("$email"),
            ),*/
           
             SizedBox(height: screensize.height/40,),
             ElevatedButton(
              onPressed: ()=>ChangePass(_textcontroller1.text.toString()),
               
              
              child: 
              const Text("पासवर्ड बदला",style: TextStyle(color: Colors.white),),
              style: ElevatedButton.styleFrom(
                backgroundColor: Colors.green
              ),
              ),
             
             
          ],
        )
        )
        )
      )
      )
    );
}
}
