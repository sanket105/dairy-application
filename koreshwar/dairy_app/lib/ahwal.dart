import 'dart:typed_data';
import 'package:flutter/material.dart';
import 'package:pdf/widgets.dart' as pw;
import 'package:printing/printing.dart';
import 'package:flutter_pdfview/flutter_pdfview.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:intl/intl.dart';

class PdfPreviewPage extends StatefulWidget {
  final String startdate;
  final String enddate;

  const PdfPreviewPage(this.startdate, this.enddate, {Key? key}) : super(key: key);

  @override
  _PdfPreviewPageState createState() => _PdfPreviewPageState(startdate, enddate);
}

class _PdfPreviewPageState extends State<PdfPreviewPage> {
  late Future<Uint8List?> _pdfFuture;
  late pw.Font _font;

  String startdate;
  String enddate;

  _PdfPreviewPageState(this.startdate, this.enddate);

  @override
  void initState() {
    super.initState();
    _pdfFuture = _loadFontAndGeneratePdf();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text(
          'PDF Preview',
          style: TextStyle(color: Colors.white, fontSize: 25),
        ),
        leading: IconButton(
          onPressed: () {
            Navigator.pop(context);
          },
          icon: const Icon(
            Icons.arrow_back,
            color: Colors.white,
            size: 30,
          ),
        ),
        actions: [
          IconButton(
            onPressed: () {
              _sharePdf(context);
            },
            icon: const Icon(
              Icons.share,
              color: Colors.white,
              size: 30,
            ),
          ),
          IconButton(
            onPressed: () {
              _printPdf();
            },
            icon: const Icon(
              Icons.print,
              color: Colors.white,
              size: 30,
            ),
          ),
          const SizedBox(width: 10,),
        ],
        backgroundColor: Colors.green,
      ),
      body: FutureBuilder<Uint8List?>(
        future: _pdfFuture,
        builder: (context, snapshot) {
          if (snapshot.connectionState == ConnectionState.waiting) {
            return const Center(child: CircularProgressIndicator());
          } else if (snapshot.hasError) {
            return Center(child: Text('Error: ${snapshot.error}'));
          } else if (snapshot.hasData) {
            return PDFView(
              filePath: null,
              pdfData: snapshot.data!,
            );
          } else {
            return Container();
          }
        },
      ),
    );
  }

  Future<void> _sharePdf(BuildContext context) async {
  final pdfBytes = await _pdfFuture;
  if (pdfBytes != null) {
    await Printing.sharePdf(
      bytes: pdfBytes,
      filename: '${DateTime.now().day},${DateTime.now().month},${DateTime.now().year}.pdf',
    );
  } else {
    // Handle the case when pdfBytes is null
    print('Error: PDF data is null');
  }
}


  Future<void> _printPdf() async {
    final pdfBytes = await _pdfFuture;
    await Printing.layoutPdf(onLayout: (_) => pdfBytes!);
  }

  Future<Uint8List?> _loadFontAndGeneratePdf() async {
    _font = await _loadFont();
    final pdf = pw.Document();
    final members = await getMembers();
    final milkData = await _fetchMilkData(members);
    _addContent(pdf, members, milkData);
    return pdf.save();
  }

  Future<pw.Font> _loadFont() async {
    final fontData = await DefaultAssetBundle.of(context).load('assets/TiroDevanagariMarathi-Regular.ttf');
    return pw.Font.ttf(fontData.buffer.asByteData());
  }

  Future<void> _addContent(pw.Document pdf, List<DocumentSnapshot> members, List<List<String>> milkData) async {
    final numRowsPerPage = 25;

    for (var i = 0; i < members.length; i += numRowsPerPage) {
      final endIndex = (i + numRowsPerPage < members.length) ? i + numRowsPerPage : members.length;
      final chunk = members.sublist(i, endIndex);
      final milkChunk = milkData.sublist(i, endIndex);
      pdf.addPage(pw.Page(
        build: (context) {
          return pw.Column(
            children: [
              _buildHeader(),
              _buildContentChunk(chunk, milkChunk),
            ],
          );
        },
      ));
    }
  }

  pw.Widget _buildHeader() {
    return pw.Container(
      alignment: pw.Alignment.center,
      margin: const pw.EdgeInsets.only(bottom: 20),
      child: pw.Row(
        children: [
        pw.Text(
          '** कोरेश्वर डेअरी **',
          style: pw.TextStyle(fontSize: 27, fontWeight: pw.FontWeight.bold, font: _font),
        ),
        pw.Spacer(),
        pw.Text(
          'तारीख : ',
          style: pw.TextStyle(fontSize: 15,font: _font),
        ),
        
            pw.Text(
              "${DateFormat.yMMMd().format(DateTime.parse(startdate))} ",
              style: const pw.TextStyle(fontSize: 15,),
            ),
            pw.Text(
              " ते ",
              style: pw.TextStyle(fontSize: 15,font: _font),
            ),
            pw.Text(
              "${DateFormat.yMMMd().format(DateTime.parse(enddate))}",
              style: const pw.TextStyle(fontSize: 15,),
            ),
          
        ],
      )
    );
  }

  pw.Widget _buildContentChunk(List<DocumentSnapshot> chunk, List<List<String>> milkChunk) {
    final List<pw.TableRow> rows = [];

    // Add header row
    rows.add(pw.TableRow(
      children: [
        pw.Container(
          alignment: pw.Alignment.center,
          height: 25,
          child: pw.Text('Code', style: pw.TextStyle(font: _font, fontWeight: pw.FontWeight.bold, fontSize: 15)),
        ),
        pw.Container(
          alignment: pw.Alignment.center,
          height: 25,
          child: pw.Text('Name', style: pw.TextStyle(font: _font, fontWeight: pw.FontWeight.bold, fontSize: 15)),
        ),
        pw.Container(
          alignment: pw.Alignment.center,
          height: 24,
          child: pw.Text('Morning', style: pw.TextStyle(font: _font, fontWeight: pw.FontWeight.bold, fontSize: 15)),
        ),
        pw.Container(
          alignment: pw.Alignment.center,
          height: 25,
          child: pw.Text('Evening', style: pw.TextStyle(font: _font, fontWeight: pw.FontWeight.bold, fontSize: 15)),
        ),
        pw.Container(
          alignment: pw.Alignment.center,
          height: 25,
          child: pw.Text(' Total ', style: pw.TextStyle(font: _font, fontWeight: pw.FontWeight.bold, fontSize: 15)),
        ),
      ],
    ));

    // Add data rows
    for (var i = 0; i < chunk.length; i++) {
      final code = chunk[i]['code'].toString();
      final name = chunk[i]['name'].toString();
      final morning = milkChunk[i][0];
      final evening = milkChunk[i][1];
      final total = (double.parse(morning) + double.parse(evening)).toString(); // Calculate total

      rows.add(pw.TableRow(
        children: [
          pw.Container(
            alignment: pw.Alignment.center,
            height: 24,
            child: pw.Text(code),
          ),
          pw.Container(
            alignment: pw.Alignment.center,
            height: 24,
            child: pw.Text(name, style: pw.TextStyle(font: _font)),
          ),
          pw.Container(
            alignment: pw.Alignment.center,
            height: 24,
            child: pw.Text(morning),
          ),
          pw.Container(
            alignment: pw.Alignment.center,
            height: 24,
            child: pw.Text(evening),
          ),
          pw.Container(
            alignment: pw.Alignment.center,
            height: 24,
            child: pw.Text(total),
          ),
        ],
      ));
    }

    return pw.Table(
      border: pw.TableBorder.all(),
      children: rows,
    );
  }

  Future<List<DocumentSnapshot>> getMembers() async {
    final querySnapshot = await FirebaseFirestore.instance.collection("Members").get();
    return querySnapshot.docs;
  }

  Future<List<List<String>>> _fetchMilkData(List<DocumentSnapshot> members) async {
    List<List<String>> milkData = [];

    for (var member in members) {
      final code = member['code'].toString();
      final morning = await fetchMilk(code, 'morningSession');
      final evening = await fetchMilk(code, 'eveningSession');
      milkData.add([morning, evening]);
    }

    return milkData;
  }

  Future<String> fetchMilk(String code, String session) async {
    DateTime startDate = DateTime.parse(startdate);
    DateTime endDate = DateTime.parse(enddate);

    if (code.isEmpty) {
      return '0';
    }

    QuerySnapshot<Map<String, dynamic>> snapshot = await FirebaseFirestore.instance
        .collection('Collection')
        .doc(code)
        .collection("milkCollection")
        .where("date", isGreaterThanOrEqualTo: startDate)
        .where("date", isLessThanOrEqualTo: endDate)
        .get();

    if (snapshot.docs.isEmpty) {
      return '0';
    }

    var totalMilk = 0.0; // Initialize total milk
    var milkDocs = snapshot.docs;

    // Iterate through all documents and sum up milk values for the correct session
    for (var doc in milkDocs) {
      var sess = doc.get('session');
      var fieldValue = doc.get('milk');

      // Check if the session matches the desired session
      if (sess == session) {
        totalMilk += fieldValue;
      }
    }

    // Display the total milk for the session
    return '$totalMilk';
  }
}
