import 'package:dairy_app/Homepage.dart';
import 'package:firebase_auth/firebase_auth.dart';
//import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/material.dart';
import 'signup.dart';
import 'forgotpass.dart';

class Login extends StatefulWidget{
  const Login({super.key});

  @override
  State<Login> createState()=>_LoginState(); 
}

class _LoginState extends State<Login>{

  var _textcontroller1=TextEditingController();
  var _textcontroller2=TextEditingController();

  bool valid=false;

  @override
  void dispose(){
    _textcontroller1.dispose();
    _textcontroller2.dispose();
    super.dispose();
  }

 // String username1="s";
  //String password1="8828";
  bool secure=true;

   Future<void> _login()async{
        if(_textcontroller1.text=="" && _textcontroller2.text==""){
                  setState(() {
                    valid=true;
                  });
                  }
                  /*else if(password1==_textcontroller2.text && username1==_textcontroller1.text){
                    _textcontroller1.clear();
                    _textcontroller2.clear();
                    valid=false;
                    Navigator.push(context,MaterialPageRoute(builder:(context)=>HomePage()));

                  }*/
                  else{
                   // UserCredential? userCredential;
                    try{
                      
                      await FirebaseAuth.instance.signInWithEmailAndPassword(email:_textcontroller1.text.toString(), password: _textcontroller2.text.toString()).then((value) => Navigator.push(context,MaterialPageRoute(builder:(context)=>HomePage())) );
                      
                    }
                    on FirebaseAuthException catch(ex){

                     // print(ex.toString());
                      var error=ex.code.toString();
                      if(error=="network-request-failed"){
                        error="इंटरनेट कनेक्शन तपासा !";
                      }
                      else{
                        error="योग्य इ-मेल,पासवर्ड प्रविष्ट करा !";
                      }
                      showDialog(context: context, 
                                builder: (ctx)=> AlertDialog(
                                  title: const Text("ERROR",style: TextStyle(color: Colors.red,fontWeight: FontWeight.bold),),
                                  content:// Text("योग्य इ-मेल,पासवर्ड प्रविष्ट करा",style: TextStyle(fontSize: 18),),
                                  Text("$error"),
                                  actions: [
                                    TextButton(onPressed: (){
                                      Navigator.of(ctx).pop();
                                    },
                                    child:const Text("ठीक आहे",style: TextStyle(fontSize: 15,color: Colors.blue),)
                                    
                                    /*Container(
                                      color: Colors.green,
                                      padding: const EdgeInsets.all(14),
                                      child: const Text("OK",st),
                                    ),*/
                                    ),
                                  ],
                                )
                                
                                );
                    }
                  }
    }

  @override
  Widget build(BuildContext context){
    var screensize=MediaQuery.of(context).size;
    return Scaffold(
      appBar: AppBar(
         title: const Text("कोरेश्वर डेअरी",style: TextStyle(color: Colors.white,fontSize: 25),),
       automaticallyImplyLeading: false,
        backgroundColor: Colors.green

      ),
      body:
      Container(
        
        height:double.infinity,
        width:double.infinity,
        decoration:BoxDecoration(image:DecorationImage(image:const AssetImage("assets/abc.webp",),fit: BoxFit.fill)),
        child:
      Center(
        child: Container(
          height: screensize.height/1.9,
          width: screensize.width/1.5,
          decoration: BoxDecoration(color: Colors.white70,borderRadius: BorderRadius.all(Radius.circular(8)),border: Border.all(color: Colors.black38)),
          child: 
        Padding(padding:EdgeInsets.all(20),
        child: 
        Column(
          children: [
            TextField(
              controller: _textcontroller1,
              decoration: InputDecoration(
                border: OutlineInputBorder(),
                hintText: "इ-मेल प्रविष्ट करा",
                errorText: valid ? "योग्य इ-मेल प्रविष्ट करा":null,
                prefixIcon: Icon(Icons.mail)
              ),
            ),
            SizedBox(height: screensize.height/55,),
            TextField(
              controller: _textcontroller2,
              decoration: InputDecoration(
                border: OutlineInputBorder(),
                hintText: "सांकेतिक अंक",
                errorText: valid ? "वापरकर्ता /सांकेतिक अंक चुकीचा":null,
                prefixIcon: Icon(Icons.password_outlined),
                suffixIcon: IconButton(
                  onPressed: (){
                     setState(() {
                       if(secure){
                        secure=false;
                       }
                       else{
                        secure=true;
                       }
                     });
                  },
                  icon:Icon(Icons.remove_red_eye),
                   
                  ) 
                //errorText: "Enter valid password"
              ),
              obscureText: secure,
              //keyboardType: TextInputType.number,
            ),
             SizedBox(height: screensize.height/40,),
             ElevatedButton(
              onPressed: ()=>_login(),
              /*  setState (()async {
                  if(_textcontroller1.text=="" || _textcontroller2.text==""){
                      valid=true;
                  }

                  /*else if(password1==_textcontroller2.text && username1==_textcontroller1.text){
                    _textcontroller1.clear();
                    _textcontroller2.clear();
                    valid=false;
                    Navigator.push(context,MaterialPageRoute(builder:(context)=>HomePage()));

                  }*/
                  else{
                    UserCredential? userCredential;
                    try{
                      userCredential=await FirebaseAuth.instance.createUserWithEmailAndPassword(email:_textcontroller1.text, password: _textcontroller2.text).then((value) => Navigator.push(context,MaterialPageRoute(builder:(context)=>HomePage())) );
                    }
                    on FirebaseAuthException catch(ex){
                      showDialog(context: context, 
                                builder: (ctx)=> AlertDialog(
                                  title: const Text("ERROR"),
                                  content: const Text("Something went wrong"),
                                  actions: [
                                    TextButton(onPressed: (){
                                      Navigator.of(ctx).pop();
                                    },
                                    child:Container(
                                      color: Colors.green,
                                      padding: const EdgeInsets.all(14),
                                      child: const Text("OK"),
                                    ),
                                    ),
                                  ],
                                )
                                
                                );
                    }
                  }
                });
              },*/
              child: 
              const Text("लॉगिन करा",style: TextStyle(color: Colors.white),),
              style: ElevatedButton.styleFrom(
                backgroundColor: Colors.green
              ),
              ),
              SizedBox(height: screensize.height/300,),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children:[
              const Text("खाते नाही आहे?"),
              TextButton(
              onPressed: (){
                setState(() {
                  Navigator.pushReplacement(context, MaterialPageRoute(builder:(context)=>Signup()));
                });
              }, 
              child: const Text("खाते बनवा",style:TextStyle(color: Colors.blue,),)
              )
                ]
              ),
               TextButton(
              onPressed: (){
                setState(() {
                  Navigator.push(context, MaterialPageRoute(builder:(context)=>ForgotPass()));
                });
              }, 
              child: const Text("पासवर्ड विसरला ?",style:TextStyle(color: Colors.blue,),)
              )
          ],
        )
        )
        )
      )
      )
    );
}
}
