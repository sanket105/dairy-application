import 'package:flutter/material.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
//import 'package:intl/intl.dart';

class Audit extends StatefulWidget{
  final String code;
  final String name;
  const Audit(this.code,this.name,{super.key});
  

  @override
  State<Audit> createState() => _AuditState(code,name);
}

class _AuditState extends State<Audit>{
  String code;
  String name;

  _AuditState(this.code,this.name);


  DateTime? _startDate;
  DateTime? _endDate;

  Future<void> _selectStartDate(BuildContext context) async {
    final DateTime? picked = await showDatePicker(
      context: context,
      initialDate: _startDate ?? DateTime.now(),
      firstDate: DateTime(2010),
      lastDate: DateTime(2101),
    );
    if (picked != null && picked != _startDate) {
      setState(() {
        _startDate = picked;
        startdate.text=_startDate.toString();
      });
    }
  }

  Future<void> _selectEndDate(BuildContext context) async {
    final DateTime? picked = await showDatePicker(
      context: context,
      initialDate: _endDate ?? DateTime.now(),
      firstDate: DateTime(2010),
      lastDate: DateTime(2101),
    );
    if (picked != null && picked != _endDate) {
      setState(() {
        _endDate = picked;
        enddate.text=_endDate.toString();
      });
    }
  }

  


 
 /* Widget fetchMilk(String code,String date,String session,String day) {
    if (code.isEmpty) {
      return const Text(" 0",style: TextStyle(color: Colors.black, fontWeight: FontWeight.bold, fontSize: 20),);
    }

    return FutureBuilder<DocumentSnapshot>(
      future: FirebaseFirestore.instance
        .collection('Collection')
        .doc(code)
        .collection("milkCollection")
        .where("date", isGreaterThanOrEqualTo: startDate)
        .where("date", isLessThanOrEqualTo: endDate)
        .get(),//FirebaseFirestore.instance.collection('Collection').doc(code).collection("milkCollection").doc().get(),
      builder: (BuildContext context, AsyncSnapshot<DocumentSnapshot> snapshot) {
        if (snapshot.connectionState == ConnectionState.waiting) {
          return const CircularProgressIndicator();
        }
        if (snapshot.hasError) {
          return const Text('0',style: TextStyle(color: Colors.red, fontWeight: FontWeight.w600, fontSize: 20),);
        }
        if (!snapshot.hasData || !snapshot.data!.exists) {
          return const Text('0',style: TextStyle(color: Colors.indigo, fontWeight: FontWeight.w600, fontSize: 20),);
        }
        var fieldValue = snapshot.data!.get('milk');
        return Text('$fieldValue',style: const TextStyle(color: Colors.indigo, fontWeight: FontWeight.w600, fontSize: 20),);
      },
    );
  }*/

  Widget fetchMilk(String code,String date,String session,String day ) {

     DateTime startDate=DateTime.parse(startdate.text);
    DateTime endDate=DateTime.parse(enddate.text);

    if (code.isEmpty) {
    return const Text(
      "0",
      style: TextStyle(color: Colors.black, fontWeight: FontWeight.bold, fontSize: 20),
    );
  }

  return FutureBuilder<QuerySnapshot<Map<String, dynamic>>>(
    future: FirebaseFirestore.instance
        .collection('Collection')
        .doc(code)
        .collection("milkCollection")
        .where("date", isGreaterThanOrEqualTo: startDate)
        .where("date", isLessThanOrEqualTo: endDate)
        .get(),
    builder: (BuildContext context, AsyncSnapshot<QuerySnapshot<Map<String, dynamic>>> snapshot) {
      if (snapshot.connectionState == ConnectionState.waiting) {
        return const CircularProgressIndicator();
      }
      if (snapshot.hasError) {
        return Text(
          '${snapshot.error}',
          style: const TextStyle(color: Colors.red, fontWeight: FontWeight.w600, fontSize: 5),
        );
      }
      if (!snapshot.hasData || snapshot.data!.docs.isEmpty) {
        return const Text(
          '0',
          style: TextStyle(color: Colors.indigo, fontWeight: FontWeight.w600, fontSize: 20),
        );
      }

      var totalMilk = 0.0; // Initialize total milk
      var milkDocs = snapshot.data!.docs;

      // Iterate through all documents and sum up milk values for the correct session
      for (var doc in milkDocs) {
        var sess = doc.get('session');
        var fieldValue = doc.get('milk');
        
        // Check if the session matches the desired session
        if (sess == session) {
          totalMilk += fieldValue;
        }
      }

      // Display the total milk for the session
      return Text(
        '$totalMilk',
        style:const TextStyle(color: Colors.indigo, fontWeight: FontWeight.w600, fontSize: 20),
      );
    },
  );
}


  

  //var dateFormat=DateFormat('dd-MM-yyyy').format(DateTime.now());

 var startdate=TextEditingController(text:"${DateTime(DateTime.now().year,DateTime.now().month,DateTime.now().day,)}"); //DateFormat('dd/MM/yyyy').format(DateTime.now()));
  var enddate=TextEditingController(text: "${DateTime(DateTime.now().year,DateTime.now().month,DateTime.now().day,)}");//DateFormat('dd/MM/yyyy').format(DateTime.now()));


fetchTotalMilk(String code) async {
  DateTime startDate=DateTime.parse(startdate.text);
  DateTime endDate=DateTime.parse(enddate.text);
  
  var collectionRef = FirebaseFirestore.instance.collection("Collection").doc(code).collection("milkCollection");

  
  var querySnapshot = await collectionRef.where("date", isGreaterThanOrEqualTo: startDate)
                                          .where("date", isLessThanOrEqualTo: endDate)
                                          .get();

  
  if (querySnapshot.docs.isNotEmpty) {
    
    double totalMilk = querySnapshot.docs.fold(0, (previousValue, doc) => previousValue + doc["milk"]);

    return totalMilk;
  } else {
    return 0;
  }
}


  

  @override
  Widget build(BuildContext context){

    var screenSize=MediaQuery.of(context).size;
    var time=DateTime(DateTime.now().year,DateTime.now().month,DateTime.now().day,);
   // int temp=time.hour;

   String date="${time.day}" + " - " + "${time.month}" + " - " + "${time.year}";
    
    return Scaffold(
      appBar: AppBar(
        title: const Text("कोरेश्वर डेअरी",style: TextStyle(color: Colors.white,fontSize: 25),),
        leading:  IconButton(
            onPressed: () {
              setState(() {
                Navigator.pop(context);
              });
            },
            icon: const Icon(Icons.arrow_back,color: Colors.white,size: 30,),
            ),
        backgroundColor: Colors.green


      ),

      body: SingleChildScrollView(
        child: Padding(
          padding: const EdgeInsets.all(20),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              const Text(
                "  तारीख",
                style: TextStyle(color: Colors.black, fontWeight: FontWeight.bold, fontSize: 20),
              ),
               Row(
                  children: [

                       
                SizedBox(
                  height: screenSize.height / 12,
                width: screenSize.width / 2.66,
                child: TextField(
                controller: startdate,
                decoration: InputDecoration(
                  border: const OutlineInputBorder(),
                   suffixIcon: IconButton(
                    onPressed: ()=> _selectStartDate(context),
                    icon: const Icon(Icons.calendar_month_outlined,color: Colors.indigo,size: 28,),
                  )
                  
                ),
                keyboardType: TextInputType.number,
              ),
                ),
                SizedBox(width: screenSize.width/20),

                const Text("ते",style: TextStyle(color: Colors.black,fontSize: 22),),
                SizedBox(width: screenSize.width/20),

               SizedBox(
                  height: screenSize.height / 12,
                width: screenSize.width / 2.66,
                child: TextField(
                controller: enddate,
                decoration: InputDecoration(
                  border: const OutlineInputBorder(),
                  suffixIcon: IconButton(
                    onPressed: ()=> _selectEndDate(context),
                    icon: const Icon(Icons.calendar_month_outlined,color: Colors.indigo,size: 28,),
                  )
                  
                ),
                keyboardType: TextInputType.number,
              ),
                ),
              
                ]
               
              ),

            
              SizedBox(height: screenSize.height / 55),
              const Text(
                "  कोड",
                style: TextStyle(color: Colors.black, fontWeight: FontWeight.bold, fontSize: 20),
              ),
              Container(
                height: screenSize.height / 16,
                width: screenSize.width / 1,
                alignment: Alignment.center,
                decoration: BoxDecoration(color: Colors.white, border: Border.all(color: Colors.black12)),
                child: Text(code,style: const TextStyle(color: Colors.indigo,fontSize: 20,),),
              ),
              SizedBox(height: screenSize.height / 55),
              const Text(
                "  सभासदाचे नाव",
                style: TextStyle(color: Colors.black, fontWeight: FontWeight.bold, fontSize: 20),
              ),
              Container(
                height: screenSize.height / 16,
                width: screenSize.width / 1,
                alignment: Alignment.center,
                decoration: BoxDecoration(color: Colors.white, border: Border.all(color: Colors.black12)),
                child: Text(name,style: const TextStyle(color: Colors.indigo,fontSize: 20,fontWeight: FontWeight.w600,),),
                
              ),
             
              SizedBox(height: screenSize.height / 55),
              const Text(
                "  आजचे दूध",
                style: TextStyle(color: Colors.black, fontWeight: FontWeight.bold, fontSize: 20),
              ),
               Container(
                height: screenSize.height / 17,
                width: screenSize.width / 1,
                alignment: Alignment.centerLeft,
                decoration: BoxDecoration(color: Colors.white, border: Border.all(color: Colors.black12)),
                child: Row(
                  children: [
                    const  Text(
                        "  ☀ सकाळ दूध        =   ",
                        style: TextStyle(color: Colors.indigo, fontSize: 22),
                    ),
                    fetchMilk(code, DateTime.now().toString(), "morningSession",date),
                    
                  ],
                )
                
                
              ),
              SizedBox(height: screenSize.height / 70),
              Container(
                height: screenSize.height / 17,
                width: screenSize.width / 1,
                alignment: Alignment.centerLeft,
                decoration: BoxDecoration(color: Colors.white, border: Border.all(color: Colors.black12)),
                child: Row(
                  children:[
                const Text(
                "  🌙 संध्याकाळ दूध   =   ",
                style: TextStyle(color: Colors.indigo, fontSize: 22),
              ),
                  fetchMilk(code,DateTime.now().toString() , "eveningSession",date),
                  ]
                ),
                
              ),
              SizedBox(height: screenSize.height / 55),
              const Text(
                "  एकूण दूध",
                style: TextStyle(color: Colors.black, fontWeight: FontWeight.bold, fontSize: 20),
              ),
               Container(
                height: screenSize.height / 17,
                width: screenSize.width / 1,
                alignment: Alignment.center,
                decoration: BoxDecoration(color: Colors.white, border: Border.all(color: Colors.black12)),
              // child:fetchTotalMilk(code),
              child: FutureBuilder(
        future: fetchTotalMilk(code),
        builder: (context, snapshot) {
          if (snapshot.connectionState == ConnectionState.waiting) {
            return const CircularProgressIndicator();
          } else if (snapshot.hasError) {
            return Text("Error: ${snapshot.error}");
          } else {
            double totalMilk=double.parse(snapshot.data.toString());

            return Text(totalMilk.toStringAsFixed(1),style: const TextStyle(color: Colors.indigo,fontSize: 20,fontWeight: FontWeight.w600),); // Return the Text widget from the future
          }
        },
      ),

                
              ),
                
              
             
            ],
          ),
        ),
      ),

      

    );
  }
}


